import React from "react";
import "./App.css";
// import Items from "./components/Items/Items";
// import User from "./components/User/User";
// import Counter from "./components/Counter/Counter";
import Posts from "./components/Posts/Posts";

function App() {
  // const items = ["Shark", "Dolphin", "Octopus"];
  return (
    <div className="App">
      {/* <User /> */}
      {/* <Counter /> */}
      <Posts />
      {/* <Items items={items} /> */}
    </div>
  );
}

export default App;
