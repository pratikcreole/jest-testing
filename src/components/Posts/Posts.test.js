import { render, screen, waitFor, fireEvent } from "@testing-library/react";
import Posts from "./Posts";

const mockPostData = [
  {
    userId: 1,
    id: 1,
    title:
      "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
    body: "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto",
  },
  {
    userId: 1,
    id: 2,
    title: "qui est esse",
    body: "est rerum tempore vitae\nsequi sint nihil reprehenderit dolor beatae ea dolores neque\nfugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis\nqui aperiam non debitis possimus qui neque nisi nulla",
  },
  {
    userId: 1,
    id: 3,
    title: "ea molestias quasi exercitationem repellat qui ipsa sit aut",
    body: "et iusto sed quo iure\nvoluptatem occaecati omnis eligendi aut ad\nvoluptatem doloribus vel accusantium quis pariatur\nmolestiae porro eius odio et labore et velit aut",
  },
];

const mockNewPostData = {
  userId: 2,
  id: 13,
  title: "Hello this is new post title",
  body: "Hello this is new post body",
};

describe("Posts", () => {
  beforeEach(() => {
    global.fetch = jest.fn((url, options) => {
      if (options?.method === "POST") {
        return Promise.resolve({
          json: () => Promise.resolve(mockNewPostData),
        });
      } else {
        return Promise.resolve({
          json: () => Promise.resolve(mockPostData),
        });
      }
    });
  });

  test("Fetch and render posts", async () => {
    render(<Posts />);
    await waitFor(() => {
      mockPostData.forEach((post) =>
        expect(screen.getByText(post.title)).toBeInTheDocument()
      );
    });
  });

  test("Click on cancel should hide form and reset input values to default", async () => {
    render(<Posts />);
    const addNewPostButton = screen.getByText("Add new post");
    await waitFor(() => {
      fireEvent.click(addNewPostButton);
    });
    const newPostTitle = screen.getByPlaceholderText("Post title");
    expect(newPostTitle).toBeInTheDocument();
    fireEvent.change(newPostTitle, {
      target: { value: "New post title" },
    });
    expect(newPostTitle.value).toBe("New post title");
    const cancelButton = screen.getByText("Cancel");
    fireEvent.click(cancelButton);
    expect(newPostTitle).not.toBeInTheDocument();
    fireEvent.click(screen.getByText("Add new post"));
    expect(screen.queryByPlaceholderText("Post title").value).toBe("");
  });

  test("Create and render a new post and submit a form works correctly", async () => {
    render(<Posts />);
    await waitFor(() => {
      fireEvent.click(screen.getByText("Add new post"));
    });
    const inputPostTitleEl = screen.queryByPlaceholderText("Post title");
    const inputPostDescriptionEl =
      screen.queryByPlaceholderText("Post Description");
    const submitPostEL = screen.getByRole("button", { name: "Submit" });
    const cancelPostEl = screen.getByRole("button", { name: "Cancel" });
    expect(inputPostTitleEl.value).toBe("");
    expect(inputPostDescriptionEl.value).toBe("");
    expect(submitPostEL).toBeInTheDocument();
    expect(cancelPostEl).toBeInTheDocument();
    fireEvent.change(inputPostTitleEl, {
      target: { value: mockNewPostData.title },
    });
    fireEvent.change(inputPostDescriptionEl, {
      target: { value: mockNewPostData.body },
    });
    await waitFor(() => {
      fireEvent.click(submitPostEL);
    });
    expect(inputPostTitleEl).not.toBeInTheDocument();
    expect(inputPostDescriptionEl).not.toBeInTheDocument();
    expect(screen.getByText(mockNewPostData.title)).toBeInTheDocument();
    expect(screen.getByText(mockNewPostData.body)).toBeInTheDocument();
  });
});
