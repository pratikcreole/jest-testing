import React, { useEffect, useState } from "react";
import "./Posts.css";

function Posts() {
  const [posts, setPosts] = useState([]);
  const [isFormVisible, setIsFormVisible] = useState(false);
  const [newPost, setNewPost] = useState({ title: "", body: "" });
  const newPostDefaultValues = { title: "", body: "" };

  useEffect(() => {
    fetch("https://jsonplaceholder.typicode.com/posts")
      .then((response) => response.json())
      .then((data) => setPosts(data));
  }, []);

  const handleSubmit = (e) => {
    e.preventDefault();
    fetch("https://jsonplaceholder.typicode.com/posts", {
      method: "POST",
      body: JSON.stringify(newPost),
      headers: {
        "Content-type": "application/json; charset=UTF-8",
      },
    })
      .then((response) => response.json())
      .then((json) => {
        setPosts([...posts, json]);
        setNewPost(newPostDefaultValues);
        setIsFormVisible(false);
      });
  };

  const handleCancel = () => {
    setNewPost(newPostDefaultValues);
    setIsFormVisible(false);
  };
  console.log("Hello");
  return (
    <div className="post-container">
      {!isFormVisible && (
        <button onClick={() => setIsFormVisible(true)}>Add new post</button>
      )}
      {isFormVisible && (
        <form className="form-post" onSubmit={handleSubmit}>
          <h3>New post</h3>
          <input
            type="text"
            placeholder="Post title"
            value={newPost.title}
            onChange={(event) => {
              setNewPost({ ...newPost, title: event.target.value });
            }}
          />
          <br />
          <textarea
            placeholder="Post Description"
            value={newPost.body}
            onChange={(event) => {
              setNewPost({ ...newPost, body: event.target.value });
            }}
          />
          <br />
          <button type="submit">Submit</button>
          <button type="button" onClick={handleCancel}>
            Cancel
          </button>
        </form>
      )}

      <h1>ALL POSTS</h1>
      <ul>
        {posts.map((post, index) => (
          <li key={index} className="post">
            <h3>{post.title}</h3>
            <p>{post.body}</p>
          </li>
        ))}
      </ul>
    </div>
  );
}

export default Posts;
