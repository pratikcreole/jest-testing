import React from "react";

function UserDetail(props) {
  const { name, email } = props.user;

  return (
    <div>
      <h3>{name}</h3>
      <span>{email}</span>
      <p style={{ color: "red" }}>API is down</p>
      <span>No test case for this text</span>
    </div>
  );
}

export default UserDetail;
