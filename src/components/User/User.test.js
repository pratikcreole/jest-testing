import {
  render,
  screen,
  waitForElementToBeRemoved,
} from "@testing-library/react";
import User from "./User";

beforeEach(() => {
  render(<User />);
});

describe("Testing App Component", () => {
  test("loading text is shown while API request is in progress", async () => {
    const loading = screen.getByText("Loading...");
    expect(loading).toBeInTheDocument();
    await waitForElementToBeRemoved(() => screen.getByText("Loading..."));
  });

  test("user's name is rendered", async () => {
    const userName = await screen.findByText("Leanne Graham");
    expect(userName).toBeInTheDocument();
  });

  test("error message is shown", async () => {
    const errorMessage = await screen.findByText("API is down");
    expect(errorMessage).toBeInTheDocument();
  });
});
