import React from "react";
import UserDetail from "./UserDetail";

function User() {
  const [user, setUser] = React.useState(null);
  const [error, setError] = React.useState("");

  React.useEffect(() => {
    fetch("https://jsonplaceholder.typicode.com/users/1")
      .then((response) => response.json())
      .then((user) => setUser(user))
      .catch((error) => setError(error.message));
  }, []);

  if (error) {
    return <span>{error}</span>;
  }

  return (
    <div>{user ? <UserDetail user={user} /> : <span>Loading...</span>}</div>
  );
}

export default User;
