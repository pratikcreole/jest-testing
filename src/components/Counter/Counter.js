import React, { useState } from "react";
import "./Counter.css";

function Counter() {
  const [counterValue, setCounterValue] = useState(0);
  const [inputValue, setInputValue] = useState(1);
  const increment = () => {
    setCounterValue(counterValue + inputValue);
  };
  const decrement = () => {
    setCounterValue(counterValue - inputValue);
  };
  const onInputNumberChange = (e) => {
    setInputValue(parseInt(e.target.value));
  };

  return (
    <div>
      <h1 data-testid="counter-header">Counter</h1>
      <h2
        data-testid="counter"
        className={`${counterValue >= 100 ? "green-color" : ""}${
          counterValue <= -100 ? "red-color" : ""
        }`}
      >
        {counterValue}
      </h2>
      <button data-testid="decrement-button" onClick={decrement}>
        -
      </button>
      <input
        data-testid="input-number"
        type="number"
        value={inputValue}
        className="input-number"
        onChange={onInputNumberChange}
      />
      <button data-testid="increment-button" onClick={increment}>
        +
      </button>
    </div>
  );
}

export default Counter;
