import Counter from "../Counter";
import { render, fireEvent } from "@testing-library/react";

let getByTestId;

beforeEach(() => {
  const component = render(<Counter />);
  getByTestId = component.getByTestId;
});

describe("Testing Counter Component", () => {
  test("Header renders with correct text", () => {
    const counterHeader = getByTestId("counter-header");
    expect(counterHeader.textContent).toBe("Counter");
  });

  test("Counter initially starts with text 0", () => {
    const counterNumber = getByTestId("counter");
    expect(counterNumber.textContent).toBe("0");
  });

  test("Input contains initial value of 1", () => {
    const inputNumber = getByTestId("input-number");
    expect(inputNumber.value).toBe("1");
  });

  test("Increment counter button renders with +", () => {
    const incrementButton = getByTestId("increment-button");
    expect(incrementButton.textContent).toBe("+");
  });

  test("Decrement counter button renders with -", () => {
    const decrementButton = getByTestId("decrement-button");
    expect(decrementButton.textContent).toBe("-");
  });

  test("Change value of input works correct", () => {
    const inputNumber = getByTestId("input-number");
    fireEvent.change(inputNumber, {
      target: {
        value: "5",
      },
    });
    expect(inputNumber.value).toBe("5");
  });

  test("Clicks on plus button add 1 to counter", () => {
    const incrementButton = getByTestId("increment-button");
    const counterNumber = getByTestId("counter");
    expect(counterNumber.textContent).toBe("0");
    fireEvent.click(incrementButton);
    expect(counterNumber.textContent).toBe("1");
  });

  test("Clicks on subtract button subtract 1 from counter", () => {
    const decrementButton = getByTestId("decrement-button");
    const counterNumber = getByTestId("counter");
    expect(counterNumber.textContent).toBe("0");
    fireEvent.click(decrementButton);
    expect(counterNumber.textContent).toBe("-1");
  });

  test("Changing input value then clicking on increment button works correctly", () => {
    const incrementButton = getByTestId("increment-button");
    const counterNumber = getByTestId("counter");
    const inputNumber = getByTestId("input-number");
    fireEvent.change(inputNumber, { target: { value: "5" } });
    fireEvent.click(incrementButton);
    expect(counterNumber.textContent).toBe("5");
  });

  test("Changing input value then clicking on decrement button works correctly", () => {
    const decrementButton = getByTestId("decrement-button");
    const counterNumber = getByTestId("counter");
    const inputNumber = getByTestId("input-number");
    fireEvent.change(inputNumber, { target: { value: "5" } });
    fireEvent.click(decrementButton);
    expect(counterNumber.textContent).toBe("-5");
  });

  test("Incrementing and then decrementing leads to the correct counter number", () => {
    const incrementButton = getByTestId("increment-button");
    const decrementButton = getByTestId("decrement-button");
    const counterNumber = getByTestId("counter");
    const inputNumber = getByTestId("input-number");
    fireEvent.change(inputNumber, { target: { value: "10" } });
    fireEvent.click(incrementButton);
    fireEvent.click(incrementButton);
    fireEvent.click(incrementButton);
    fireEvent.click(incrementButton);
    fireEvent.click(decrementButton);
    fireEvent.click(decrementButton);
    expect(counterNumber.textContent).toBe("20");
    fireEvent.change(inputNumber, { target: { value: "20" } });
    fireEvent.click(decrementButton);
    fireEvent.click(decrementButton);
    expect(counterNumber.textContent).toBe("-20");
  });

  test("Counter contains correct className", () => {
    const incrementButton = getByTestId("increment-button");
    const decrementButton = getByTestId("decrement-button");
    const counterNumber = getByTestId("counter");
    const inputNumber = getByTestId("input-number");
    expect(counterNumber.className).toBe("");
    fireEvent.change(inputNumber, { target: { value: "50" } });
    fireEvent.click(incrementButton);
    expect(counterNumber.className).toBe("");
    fireEvent.click(incrementButton);
    expect(counterNumber.className).toBe("green-color");
    fireEvent.click(incrementButton);
    expect(counterNumber.className).toBe("green-color");
    fireEvent.click(decrementButton);
    fireEvent.click(decrementButton);
    expect(counterNumber.className).toBe("");
    fireEvent.click(decrementButton);
    expect(counterNumber.className).toBe("");
    fireEvent.click(decrementButton);
    expect(counterNumber.className).toBe("");
    fireEvent.click(decrementButton);
    expect(counterNumber.className).toBe("red-color");
  });
});
